#!/usr/bin/env python3
"""Requires dnspython 2.2.0+"""

import os
import sys
from dns import resolver, rdatatype, exception
from dns.rdtypes import svcbbase

MYNAME = os.path.basename(__file__)

try:
    QNAME = sys.argv[1]
except IndexError:
    print(f"Missing domain name.\n\nUsage: {MYNAME} <domain name>", file=sys.stderr)
    sys.exit(1)

def svcb_params_to_text(params: dict) -> str:
    """Format svcb params from dict to nice output"""
    # https://github.com/rthalley/dnspython/blob/master/dns/rdtypes/svcbbase.py
    # As of draft-ietf-dnsop-svcb-https-10, only SvcParamKey O, 1, 4 & 6 can have multiple values,
    # but we are lazy and the to_text() functions from svcbbase are OK
    # so we format them the same way no matter the SvcParamKey
    params_str = ""
    for key,value in params.items():
        if key == 7:
            param_key = "dohpath"
        else:
            param_key = svcbbase.key_to_text(key)
        param_list = ' & '.join(value.to_text().replace("\"","").rsplit(",",1)).replace(",",", ")
        params_str += f"\n\t\t- {param_key}: {param_list} "
    return params_str


def print_svcb_rr(name: str) -> str:
    """Main func : takes a domain name and prints SVCB & HTTPS records if any"""
    output = ""
    name_list = [ name ]
    pos = 0
    while pos < len(name_list):
        dns_error = False
        no_ans = 0
        bold_name = "\033[1m" + name_list[pos]  + "\033[0m"
        output += f"\nSVCB/HTTPS records for {bold_name}:\n\n"
        for qtype in ("SVCB", "HTTPS"):
            try:
                answers = resolver.resolve(name_list[pos], qtype)
            except resolver.NoAnswer:
                output += f"No {qtype} record found.\n"
                no_ans += 1
            except resolver.NXDOMAIN:
                output += f"ERROR:The name {bold_name} does not exist."
                dns_error = True
                break
            # Lazy catch all
            except exception.DNSException as error:
                output += f"ERROR! {error}"
                dns_error = True
                break
        if no_ans == 2 or dns_error:
            pos += 1
            continue
        rrset_len = len(answers.rrset)
        for rdata in answers:
            if rdata.target.to_text() == ".":
                target = ". (self)"
            else:
                target = rdata.target
                if target.to_text() not in name_list:
                    name_list = [*name_list, target.to_text()]
            if rdata.priority == 0:
                priority = "0 (Alias Mode)"
            else:
                priority = f"{rdata.priority} (Service Mode)"
            if rdata.rdtype == rdatatype.HTTPS:
                output += "\nService Binding with HTTP record found:\n"
            else:
                output += "\nService Binding record found:\n"
            output += f"\t- Priority: {priority}\n"
            output += f"\t- Target: {target}\n"
            if rdata.params:
                params = svcb_params_to_text(rdata.params)
                output += f"\t- Params: {params}\n"
            if rrset_len == 1:
                pos += 1
            rrset_len -= 1
    return output

print(print_svcb_rr(QNAME))
