# Dépotoir de Shaft Inc.

Des bouts de scripts inutiles et des trucs cassés issus de mes apprentissages.

Sauf indications contraires, tout est sous Licence Publique IV : voir [LICENSE](LICENSE) ou https://www.shaftinc.fr/licence-publique-iv.html

## Contenu du dépotoir

### DNS

- [find-svcb.py](dns/find-svcb.py) : prend un nom de domaine en entrée. Cherche des enregistrements `SVCB` et `HTTPS` (voir [draft-ietf-dnsop-svcb-https](https://datatracker.ietf.org/doc/draft-ietf-dnsop-svcb-https/)) pour le domaine. Récupère les noms de domaines trouvés dans le `TargetName` des enregistrements et fait également une recherche pour ces domaines. Nécessite [dnspython](https://www.dnspython.org/) 2.1.0 ou supérieur.
